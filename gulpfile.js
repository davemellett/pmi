var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');


gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
});
 
gulp.task('scripts', function() {
  return gulp.src(['js/app.js', 'js/app-states.js', './js/controllers/*.js', './js/templates/*.js'])
    .pipe(concat('pmi.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('vendor-scripts', function() {
    return gulp.src([
        'node_modules/angular/angular.min.js',
        'node_modules/@uirouter/angularjs/release/angular-ui-router.min.js',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        'node_modules/angular-ui-grid/ui-grid.min.js',
        'node_modules/ng-tags-input/build/ng-tags-input.min.js',
        'node_modules/dropzone/dist/min/dropzone.min.js',
        'node_modules/ngdropzone/dist/ng-dropzone.min.js',
        'node_modules/angular-local-storage/dist/angular-local-storage.min.js'
    ])
      .pipe(concat('vendor.js'))
      //.pipe(uglify())
      .pipe(gulp.dest('./dist/'));
  });

  gulp.task('vendor-styles', function(){
    return gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/angular-ui-grid/ui-grid.min.css',
        'node_modules/ng-tags-input/build/ng-tags-input.min.css',
        'node_modules/ng-tags-input/build/ng-tags-input.bootstrap.min.css',
        'node_modules/dropzone/dist/min/dropzone.min.css',
        'node_modules/ngdropzone/dist/ng-dropzone.min.css'
    ])
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./dist/'));
  });

  gulp.task('sass', function(){
    return gulp.src('css/test.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.reload({
            stream: true
        }))
        ;

  });

  gulp.task('watch', ['browserSync', 'sass'], function(){
      gulp.watch('css/**/*.scss', ['sass']);
  })
  





gulp.task('build', [ 'scripts', 'vendor-scripts', 'vendor-styles', 'sass' ]);