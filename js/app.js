
angular.module('pmiApp', 
  [
    'ui.bootstrap', 
    'ui.router', 
    'ngTagsInput', 
    'ui.grid', 
    'thatisuday.dropzone',
    'LocalStorageModule'
  ]
  );

Dropzone.autoDiscover = false;

angular.module('pmiApp').config(['localStorageServiceProvider', function (localStorageServiceProvider) {
  localStorageServiceProvider.setPrefix('pmi-app');
  localStorageServiceProvider.setStorageType('localStorage');
  localStorageServiceProvider.setNotify(true, true);
}]);  





