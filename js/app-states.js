angular.module('pmiApp').config(['$stateProvider', function($stateProvider){

    var states = [
        {
            name: 'datePicker',
            params: {
                url: '/datePicker',
                templateUrl: 'js/templates/datePicker.html',
                controller: 'DatepickerDemoCtrl'
            }
        },
        {
            name: 'tagsInput',
            params: {
                url: '/tagsInput',
                templateUrl: 'js/templates/tagsInput.html',
                controller: 'TagsInputCtrl'
            }
        },
        {
            name: 'todoList',
            params: {
                url: '/todoList',
                templateUrl: 'js/templates/todoList.html',
                controller: 'TodoListController',
                controllerAs: 'todoList'
            }
        },
        {
            name: 'grid',
            params: {
                url: '/grid',
                templateUrl: 'js/templates/ui-grid.html',
                controller: 'GridCtrl'
            }
        },
        {
            name: 'dropzone',
            params: {
                url: '/dropzone',
                templateUrl: 'js/templates/dropzone.html',
                controller: 'DropzoneCtrl'
            }
        },
        {
            name: 'dzUpload',
            params: {
                url: '/alt_upload_url',
                template: '<h1>Uploads</h1>'
            }
        },
        {
            name: 'localStorage',
            params: {
                url: '/localStorage',
                templateUrl: 'js/templates/localstorage.html',
                controller: 'LocalStorageCtrl'
            }
        },
        {
            name: "modalDialog",
            params: {
                url: '/modalDialog',
                templateUrl: 'js/templates/modalDialog.html',
                controller: 'ModalDialogCtrl',
                controllerAs: '$ctrl'
            }
        },
        {
            name: "typeahead",
            params: {
                url: '/type-ahead',
                templateUrl: 'js/templates/typeahead.html',
                controller: 'TypeAheadCtrl'
            }
        },
        {
            name: "fonto",
            params: {
                url: '/fonto',
                templateUrl: 'js/templates/fontoxml.html',
                controller: 'FontoCtrl'
            }
        }
    ];

    states.forEach(function(state){

        $stateProvider.state(state.name, state.params);

    });

}]);
