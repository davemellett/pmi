angular.module('pmiApp').controller('LocalStorageCtrl', 
['$scope', 'localStorageService', 
function($scope, localStorageService) {


    $scope.isSupported = localStorageService.isSupported ? "Yes" : "No";

    $scope.allKeys = localStorageService.keys();

    $scope.submit = function(){
            localStorageService.set($scope.theKey, $scope.theValue);
            $scope.allKeys = localStorageService.keys();
    };

    $scope.clearAll = function() {
        var all = localStorageService.clearAll();
        $scope.allKeys = localStorageService.keys();
    };

    
}]);