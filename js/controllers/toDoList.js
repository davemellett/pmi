angular.module('pmiApp').controller('TodoListController', function() {
    var todoList = this;
    todoList.todos = [
      {text:'AngularJS', done:true},
      {text:'ui-router', done:true},
      {text:'Bootstrap', done:true},
      {text:'ui-bootstrap (Datepicker)', done:true},
      {text:'ui-bootstrap (Modal)', done:true},
      {text:'ui-bootstrap (Typeahead)', done:true},
      {text:'ui-grid', done:true},
      {text:'ng-tags-input', done:true},
      {text:'ng-dropzone', done:true},
      {text:'angular-local-storage', done:true}
    ];
 
    todoList.addTodo = function() {
      todoList.todos.push({text:todoList.todoText, done:false});
      todoList.todoText = '';
    };
 
    todoList.remaining = function() {
      var count = 0;
      angular.forEach(todoList.todos, function(todo) {
        count += todo.done ? 0 : 1;
      });
      return count;
    };
 
    todoList.archive = function() {
      var oldTodos = todoList.todos;
      todoList.todos = [];
      angular.forEach(oldTodos, function(todo) {
        if (!todo.done) todoList.todos.push(todo);
      });
    };
  });
