angular.module('pmiApp').controller('TagsInputCtrl', [ '$scope', function($scope) {
    $scope.tags = [
      { text: 'just' },
      { text: 'some' },
      { text: 'cool' },
      { text: 'tags' }
    ];

    $scope.loadTags = function(query) {
      return $http.get('/tags?query=' + query);
    };
    
  }]);